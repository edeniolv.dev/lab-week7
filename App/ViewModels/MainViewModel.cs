﻿using System;
using System.Collections.ObjectModel;
using System.Reactive;
using App.Models.Entity;
using App.Models.Services;
using Avalonia.Platform.Storage;
using ReactiveUI;

namespace App.ViewModels;

public class MainViewModel : ViewModelBase
{
    public ReactiveCommand<Unit, Unit> ImportCommand { get; }
    public ReactiveCommand<Unit, Unit> ExportCommand { get; }
    public ReactiveCommand<Unit, Unit> ResetCommand { get; }
    public ReactiveCommand<Unit, Unit> UsePickerCommand { get; }
    private StockFileService Service { get; }
    public ObservableCollection<StockItem>? StockItems
    {
        get => _stockItems;
        set => this.RaiseAndSetIfChanged(ref _stockItems, value);
    }
    public bool IsMarked
    {
        get => _isMarked;
        set => this.RaiseAndSetIfChanged(ref _isMarked, value);
    }
    public bool IsEnabledImport
    {
        get => _isEnabledImport;
        set => this.RaiseAndSetIfChanged(ref _isEnabledImport, value);
    }
    public bool IsEnabledExport
    {
        get => _isEnabledExport;
        set => this.RaiseAndSetIfChanged(ref _isEnabledExport, value);
    }
    
    private ObservableCollection<StockItem>? _stockItems;
    private bool _isMarked;
    private bool _isEnabledImport = true;
    private bool _isEnabledExport;
    
    public MainViewModel(IStorageProvider storage)
    {
        Service = new StockFileService(storage);
        
        ImportCommand = ReactiveCommand.Create(OnImportBtnClick);
        ExportCommand = ReactiveCommand.Create(OnExportBtnClick);
        ResetCommand = ReactiveCommand.Create(ResetAll);
        UsePickerCommand = ReactiveCommand.Create(SetUsePicker);
    }
    
    private async void OnImportBtnClick()
    {
        try
        {
            IsEnabledImport = false;
            IsEnabledExport = true;
        
            await Service.ReadFile();
            StockItems = Service.GetItems();
        }
        catch (ArgumentOutOfRangeException exception)
        {
            string message = "No files loaded";
            Console.WriteLine(message);
            LogFileService.Instance.WriteLog(exception, message);
            ResetAll();
        }
        catch (IndexOutOfRangeException exception)
        {
            string message = "Number of columns greater than allowed, check the file";
            Console.WriteLine(message);
            LogFileService.Instance.WriteLog(exception, message);
            ResetAll();
        }
    }
    
    private async void OnExportBtnClick()
    {
        IsEnabledImport = true;
        IsEnabledExport = false;
        
        await Service.WriteFile();
        StockItems = null;
    }

    private void SetUsePicker()
    {
        if (IsMarked)
        {
            Service.Import.UsePicker = true;
            Service.Export.UsePicker = true;
        }
        else
        {
            Service.Import.UsePicker = false;
            Service.Export.UsePicker = false;
        }
    }

    private void ResetAll()
    {
        IsEnabledImport = true;
        IsEnabledExport = false;
        
        Service.Import.UsePicker = false;
        Service.Export.UsePicker = false;
        
        IsMarked = false;
        
        StockItems = null;
        Service.CleanAllItems();
    }
}
