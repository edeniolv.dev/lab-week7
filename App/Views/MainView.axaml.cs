using App.ViewModels;
using Avalonia.Controls;

namespace App.Views;

public partial class MainView : Window
{
    private MainViewModel ViewModel { get; }
    
    public MainView()
    {
        InitializeComponent();
        ViewModel = new MainViewModel(StorageProvider);
        DataContext = ViewModel;
    }
}
