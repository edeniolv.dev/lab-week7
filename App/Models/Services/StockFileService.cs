using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using App.Models.Entity;
using App.Models.Repository;
using Avalonia.Platform.Storage;

namespace App.Models.Services;

public class StockFileService : IFileService<StockItem>
{
    private StockRepository StockRepository { get; } = new();
    private IConverter<StockItem> Converter { get; } = new StockItemConverter();
    public FileImport Import { get; }
    public FileExport Export { get; }
    
    public StockFileService(IStorageProvider storage)
    {
        Import = new FileImport(storage);
        Export = new FileExport(storage);
    }

    public async Task ReadFile()
    {
        await Import.BuildImportStream();
        CleanAllItems();

        if (Import.Reader != null)
        {
            while (await Import.Reader.ReadLineAsync() is { } line)
            {
                string[] values = line.Split("|");
                StockRepository.Add(Converter.ToObject(values));
            }

            Import.Reader.Close();
            Import.Stream?.Close();
        }
        Console.WriteLine("Read all lines from archive");
    }

    public async Task WriteFile()
    {
        await Export.BuildExportStream();

        if (Export.Writer != null && Export.WriterLog != null)
        {
            foreach (StockItem item in GetItems())
            {
                await Export.Writer.WriteLineAsync(item.ToString());
            }

            foreach (Log log in LogFileService.Instance.ReadLog())
            {
                await Export.WriterLog.WriteLineAsync(log.ToString());
            }
            
            Export.Writer.Close();
            Export.WriterLog?.Close();
            Export.StreamFile?.Close();
        }
        Console.WriteLine("Export all data to archive");
        
        CleanAllItems();
    }
    
    public ObservableCollection<StockItem> GetItems()
    {
        return new ObservableCollection<StockItem>(StockRepository.ReadAll());
    }

    public void CleanAllItems()
    {
        StockRepository.Clear();
    }
}
