using System;
using System.Collections.Generic;
using App.Models.Entity;
using App.Models.Repository;

namespace App.Models.Services;

public class LogFileService
{
    public static LogFileService Instance { get; } = new();
    private LogRepository Repository { get; } = new();

    private LogFileService() { }
    
    public void WriteLog(Exception exception, string message)
    {
        Repository.Add(new Log(exception, message));
    }

    public List<Log> ReadLog()
    {
        return Repository.ReadAll();
    }

    public void ClearAllLogs()
    {
        Repository.Clear();
    }
}
