using System.Collections.Generic;
using App.Models.Entity;

namespace App.Models.Repository;

public class LogRepository
{
    private List<Log> Logs { get; } = new();

    public void Add(Log item)
    {
        Logs.Add(item);
    }

    public List<Log> ReadAll()
    {
        return Logs;
    }

    public void Clear()
    {
        Logs.Clear();
    }
}
