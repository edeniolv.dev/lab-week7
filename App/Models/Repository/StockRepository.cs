using System.Collections.Generic;
using App.Models.Entity;

namespace App.Models.Repository;

public class StockRepository
{
    private List<StockItem> Items { get; } = new();

    public void Add(StockItem item)
    {
        Items.Add(item);
    }

    public List<StockItem> ReadAll()
    {
        return Items;
    }

    public void Clear()
    {
        Items.Clear();
    }
}
