using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace App.Models;

public interface IFileService<T>
{
    public Task ReadFile();
    public Task WriteFile();
    public ObservableCollection<T> GetItems();
}