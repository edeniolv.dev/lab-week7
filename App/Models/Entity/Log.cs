using System;

namespace App.Models.Entity;

public class Log
{
    private Exception Exception { get; }
    private string Message { get; }
    private DateTime DateTime { get; } = DateTime.Now;

    public Log(Exception exception, string message)
    {
        Exception = exception;
        Message = message;
    }

    public override string ToString()
    {
        return $"Date Time: {DateTime} Exception: {Exception.GetType().Name} Message: {Message}";
    }
}
