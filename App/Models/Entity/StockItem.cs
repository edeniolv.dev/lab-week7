namespace App.Models.Entity;

public class StockItem
{
    public string? Name { get; set; }
    public long Quantity { get; set; }
    public double Price { get; set; }

    public override string ToString()
    {
        return $"{Name}|{Quantity}|{Price}";
    }
}
