using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace App.Models.Entity;

public class StockItemConverter : IConverter<StockItem>
{
    private StockItem? Item { get; set; }
    private PropertyInfo[]? Properties { get; set; }
    
    public StockItem ToObject(params string[] values)
    {
        Item = new StockItem();
        Properties = Item.GetType().GetProperties();
        
        for (int i = 0; i < values.Length; i++)
        {
            string property = values[i];
            Type type = Properties[i].PropertyType;
            
            ValidateValue(ref property, type);
            
            Properties[i].SetValue(
                Item, Convert.ChangeType(property, type)
            );
        }
        return Item;
    }
    
    private void ValidateValue(ref string value, Type type)
    {
        if (value.Contains(','))
        {
            value = value.Replace(',', '.');
        }

        if (type != typeof(string) && IsNotDigitValid(value))
        {
            value = "0";
        }

        if (type == typeof(string) && IsNotStringValid(value))
        {
            value = "null";
        }
    }

    private bool IsNotDigitValid(string value)
    {
        return 
            value.Length == 0 || 
            !Regex.IsMatch(value, @"^[0-9,\.]+$");
    }

    private bool IsNotStringValid(string value)
    {
        return 
            value.Length == 0 || 
            !Regex.IsMatch(value, @"^[a-zA-Zà-úÀ-Ú]+([\s-][a-zA-Zà-úÀ-Ú]+)*$");
    }
}
