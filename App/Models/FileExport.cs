using System.IO;
using System.Threading.Tasks;
using Avalonia.Platform.Storage;

namespace App.Models;

public class FileExport : FilePickerSaveOptions
{
    private IStorageProvider Storage { get; }
    private FilePickerFileType Types { get; } = new("csv");
    private IStorageFile? File { get; set; }
    public Stream? StreamFile { get; private set; }
    public StreamWriter? Writer { get; private set; }
    public StreamWriter? WriterLog { get; private set; }
    public bool UsePicker { get; set; }

    public FileExport(IStorageProvider storage)
    {
        Storage = storage;
        ConfigExport();
    }

    private void ConfigExport()
    {
        Types.Patterns = new[] { "*.csv" };
        FileTypeChoices = new[] { Types };
        SuggestedFileName = "new-table";
        ShowOverwritePrompt = true;
    }

    public async Task BuildExportStream()
    {
        if (UsePicker)
        {
            File = await Storage.SaveFilePickerAsync(this);
            
            if (File != null)
            {
                string? path = Path.GetDirectoryName(File.Path.LocalPath);
                
                StreamFile = await File.OpenWriteAsync();
                Writer = new StreamWriter(StreamFile);
                WriterLog = new StreamWriter(path + "/exception.log");
            }
            return;
        }
        Writer = new StreamWriter(GetPathFile("new-items.csv"));
        WriterLog = new StreamWriter(GetPathFile("exception.log"));
    }
    
    private string GetPathFile(string name)
    {
        return Path.GetFullPath("../../../Assets/") + name;
    }
}
