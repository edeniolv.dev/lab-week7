namespace App.Models;

public interface IConverter<out T>
{
    T ToObject(params string[] values);
}