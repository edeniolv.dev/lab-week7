using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Avalonia.Platform.Storage;

namespace App.Models;

public class FileImport : FilePickerOpenOptions
{
    private IStorageProvider Storage { get; }
    private FilePickerFileType Types { get; } = new("csv");
    private IReadOnlyList<IStorageFile>? File { get; set; }
    public Stream? Stream { get; private set; }
    public StreamReader? Reader { get; private set; }
    public bool UsePicker { get; set; }
    
    public FileImport(IStorageProvider storage)
    {
        Storage = storage;
        ConfigImport();
    }

    private void ConfigImport()
    {
        Types.Patterns = new[] { "*.csv", "*.txt" };
        FileTypeFilter = new[] { Types };
        AllowMultiple = false;
    }

    public async Task BuildImportStream()
    {
        if (UsePicker)
        {
            File = await Storage.OpenFilePickerAsync(this);
            
            if (File != null)
            {
                Stream = await File[0].OpenReadAsync();
                Reader = new StreamReader(Stream);
            }
            return;
        }
        Reader = new StreamReader(GetPathFile());
    }

    private string GetPathFile()
    {
        return Path.GetFullPath("../../../Assets/") + "items1.csv";
    }
}
